VERSION=2.39

.PHONY: docker all binutils/binutils-$(VERSION)

include triplets/$(VERSION).mk


all: $(TRIPLETS)
	echo "$^"

binutils/binutils-$(VERSION): archives/binutils-$(VERSION).tar.gz | binutils
	tar -C binutils -xf $^

archives/binutils-$(VERSION).tar.gz: | archives
	wget -O $@ http://ftpmirror.gnu.org/binutils/binutils-$(VERSION).tar.gz

archives binutils tmp triplets builds compiled:
	mkdir -p $@

tmp/config.bfd.0: | tmp
	@# get rid of the file parts which do not have target names
	head -n $(shell grep -nP 'END OF targmatch.h' binutils/binutils-$(VERSION)/bfd/config.bfd | cut -f1 -d:) binutils/binutils-$(VERSION)/bfd/config.bfd | \
	tail -n +$(shell grep -nP 'START OF targmatch.h' binutils/binutils-$(VERSION)/bfd/config.bfd | cut -f1 -d:) > $@

tmp/config.bfd.1: tmp/config.bfd.0
	grep -E "^  .+)$$" $^ | tr -d ' ' | tr -d ')' | \
	sed -E 's/\|/\n/g' > $@

tmp/config.bfd.2: tmp/config.bfd.1
	sed -E 's/\[([0-9]).+\]/\1/g' $^ > $@

tmp/config.bfd.3: tmp/config.bfd.2
	sed 's/[-][*][-]/-unknown-/' $^ > $@

tmp/config.bfd.4: tmp/config.bfd.3
	sed -E 's/([a-zA-Z0-9.])[*]$$/\1/' $^ | \
	sed -E 's/[-][*]$$//' > $@

tmp/config.bfd.5: tmp/config.bfd.4
	sed -E 's/([a-zA-Z0-9])[*]/\1/g' $^ | \
	sed -E 's/[*]([a-zA-Z0-9])/\1/g' > $@

tmp/config.bfd.6: tmp/config.bfd.5
	sed -E 's/[*][-]/i386-/g' $^ > $@

tmp/config.bfd.7: tmp/config.bfd.6
	sed 's/unknown$$/unknown-none/' $^ > $@

tmp/config.bfd.8: tmp/config.bfd.7
	sed 's/chorus$$/chorusrdb/'  $^ > $@

tmp/config.bfd.9: tmp/config.bfd.8
	sed 's/m68[-]/m68k-/' $^ | \
	sed 's/riscvbe/riscv32be/' | \
	sed 's/mingw$$/mingw64/' > $@

tmp/config.bfd.10: tmp/config.bfd.9
	sed '/kaos/d'  $^ > $@

tmp/config.bfd.11: tmp/config.bfd.10
	sed '/arm9e/d' $^ | \
	sed '/ntoarm/d' > $@

tmp/config.bfd.12: tmp/config.bfd.11
	sed '/armb-unknown-freebsd/d' $^ | \
	sed '/armb-unknown-linux/d' | \
	sed '/i386-unknown-knetbsd-gnu/d' | \
	sed '/sh1l-unknown-elf/d' | \
	sed '/sparcv-unknown-linux/d' | \
	sed '/ns32k-pc532-ux/d' | \
	sed '/aarch64_be-unknown-netbsd/d' | \
	sed '/arm-unknown-wince/d' > $@

# *** ld does not support target
tmp/config.bfd.13: tmp/config.bfd.12
	sed '/aarch64-unknown-netbsd/d' $^ | \
	sed '/arm-wrs-windiss/d' | \
	sed '/mipsel-unknown-openbsd/d' | \
	sed '/powerpc64-ibm-aix5.0/d' | \
	sed '/mips-unknown-openbsd/d' | \
	sed '/powerpc64-unknown-aix/d' | \
	sed '/powerpc64-ibm-aix5/d' | \
	sed '/mips-unknown-openbsd/d' | \
	sed '/mips-unknown-irix5/d' | \
	sed '/mips-unknown-irix6/d' | \
	sed '/shl-unknown-elf/d' | \
	sed '/powerpc-unknown-macos10/d' | \
	sed '/alpha-unknown-elf/d' > $@

#*** Configuration powerpc-unknown-lynxos is now obsolete
#*** and so support for it has been REMOVED.
tmp/config.bfd.14: tmp/config.bfd.13
	sed '/alpha-unknown-none/d' $^ | \
	sed '/i386-unknown-macos10/d' | \
	sed '/powerpc-unknown-rhapsody/d' | \
	sed '/x86_64-unknown-dicos/d' | \
	sed '/powerpc64-unknown-bsd/d' | \
	sed '/powerpc64le-unknown-bsd/d' | \
	sed '/mips-unknown-chorusrdb/d' | \
	sed '/powerpc-unknown-chorusrdb/d' | \
	sed '/mips-unknown-none/d' | \
	sed '/x86_64-unknown-pep/d' | \
	sed '/ns32k-unknown-lites/d' | \
	sed '/rs6000-unknown-none/d' | \
	sed '/pdp11-unknown-none/d' | \
	sed '/i386-unknown-rhapsody/d' | \
	sed '/powerpc-unknown-lynxos/d' | \
	sed '/powerpc-unknown-windiss/d' | \
	sed '/i386-unknown-dicos/d' > $@

# This target is no longer supported in gas
tmp/config.bfd.15: tmp/config.bfd.14
	sed '/bfin-unknown-none/d' $^ | \
	sed '/m32r-unknown-none/d' | \
	sed '/m68k-unknown-none/d' | \
	sed '/metag-unknown-none/d' | \
	sed '/ns32k-unknown-bsd/d' | \
	sed '/nios2eb-unknown-none/d' | \
	sed '/nios2el-unknown-none/d' | \
	sed '/nios2-unknown-none/d' | \
	sed '/c30-unknown-coff/d' | \
	sed '/nds32le-unknown-none/d' | \
	sed '/nds32be-unknown-none/d' | \
	sed '/cr16-unknown-uclinux/d' | \
	sed '/vax-unknown-netbsdaout/d' | \
	sed '/vax-unknown-openbsd/d' | \
	sed '/amdgcn-unknown-none/d' > $@

# compilation error: error: expected ‘;’ before ‘sec’
tmp/config.bfd.16: tmp/config.bfd.15
	sed '/i386-unknown-interix/d' $^ > $@ 

# bfd_m32rle_arch’ undeclared here (not in a function); did you mean ‘bfd_m32r_arch’?
tmp/config.bfd.17: tmp/config.bfd.16
	sed '/m32rle-unknown-linux/d' $^ | \
	sed '/m32rle-unknown-none/d' > $@ 

#undefined reference to `coff_section_from_bfd_index'
# configure: error: GAS does not know what format to use for target 
tmp/config.bfd.18: tmp/config.bfd.17
	sed '/sh-unknown-openbsd/d' $^ | \
	sed '/sparc64-unknown-none/d' | \
	sed '/sparc-unknown-none/d' | \
	sed '/z8k-unknown-none/d' | \
	sed '/sh-unknown-none/d' > $@ 

triplets/$(VERSION).mk: tmp/config.bfd.18 | triplets
	printf "TRIPLETS=" > $@
	sed -E 's/^(.+)$$/\1 \\/g' $^ >> $@
	truncate -s -3 $@
	echo >> $@

.PHONY: $(TRIPLETS)
$(TRIPLETS): | builds compiled
	mkdir -p builds/$(VERSION)/$@
	cd builds/$(VERSION)/$@; ../../../binutils/binutils-$(VERSION)/configure --target $@ --prefix=$(shell pwd)/compiled/$(VERSION)/$@/usr
	cd builds/$(VERSION)/$@; make
	mkdir -p compiled/$(VERSION)/$@/usr
	cd builds/$(VERSION)/$@; make install

DOCKER_TAG=aa

docker:
	docker run -u $(shell id -u):$(shell id -g) --rm -v $(shell pwd):/w -w /w -it $(DOCKER_TAG)

docker-build:
	docker build -t $(DOCKER_TAG) .

2.39:
	docker run -u $(shell id -u):$(shell id -g) --rm -v $(shell pwd):/w -w /w -t $(DOCKER_TAG) ./scripts/dl-build.sh $@
