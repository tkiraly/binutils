FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get -y install wget gcc make file flex bison texinfo g++

ENTRYPOINT [ "bash" ]
